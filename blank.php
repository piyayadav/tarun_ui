<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Blank</title>
	<?php include("includes/common/scripts.php"); ?>
</head>
<body class="sidebar-xs">
	<?php include("includes/common/topbar.php"); ?>
	<div class="page-container">
		<div class="page-content">
			<?php include("includes/common/siderbar.php"); ?>
			<?php include("includes/header/page1-header.php"); ?>
			<!-- Page Content Start -->
			<div class="content">
				<!-- middle part goes here -->
			</div>
			<!-- End Content -->
			<!-- Page Content End -->
			<?php include("includes/common/footer.php"); ?>
		</div>
	</div>
</body>
</html>