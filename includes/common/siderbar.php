<div class="sidebar sidebar-main">
	<div class="sidebar-content">
		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">
                    <li class="divider"><span></span></li>
					<li>
						<a href="#"><i class="icon-stack2"></i> <span>Static Pages</span></a>
						<ul>
							<li><a href="layout_navbar_fixed.html">Fixed navbar</a></li>
							<li><a href="layout_navbar_sidebar_fixed.html">Fixed navbar &amp; sidebar</a></li>
							<li><a href="layout_sidebar_fixed_native.html">Fixed sidebar native scroll</a></li>
							<li><a href="layout_navbar_hideable.html">Hideable navbar</a></li>
						</ul>
					</li>
                    <li><a href="#"><i class="icon-embed"></i> <span>HTML Fangate Tabs</span></a></li>
                    <li><a href="#"><i class="icon-pinterest2"></i> <span>Pinterest Tabs</span> <span class="badge s-count">1</span></a></li>
                    <li><a href="#"><i class="icon-twitter"></i> <span>Twitter Tabs</span> <span class="badge s-count">5</span></a></li>
                    <li><a href="#"><i class="icon-instagram"></i> <span>Instagram Tabs <span class="badge s-count">10</span></span></a></li>
                    <li><a href="#"><i class="icon-youtube"></i> <span>Youtube Tabs <span class="badge s-count">2</span></span></a></li>
                    
				</ul>
			</div>
		</div>
		<!-- /main navigation -->
	</div>
</div>