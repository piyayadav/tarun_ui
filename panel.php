<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Panel</title>
	<?php include("includes/common/scripts.php"); ?>
</head>
<body class="sidebar-xs">
	<?php include("includes/common/topbar.php"); ?>
	<div class="page-container">
		<div class="page-content">
			<?php include("includes/common/siderbar.php"); ?>
			<?php include("includes/header/page1-header.php"); ?>
			<!-- Page Content Start -->
			<div class="content">
				<!-- Column ordering -->
				<div class="panel panel-flat">
					<!-- Start Panel -->
					<div class="panel-heading">
						<h5 class="panel-title">Panel Title</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="grid-demo">
							<div class="row rows-24">
								<div class="col-24-md-8">
									Placeholder
								</div>
								<div class="col-24-md-8">
									Placeholder
								</div>
								<div class="col-24-md-8">
									Placeholder
								</div>
							</div>
						</div>
					</div>
					<!-- End Panel Body -->
				</div>
				<!-- End Panel -->
			</div>
			<!-- End Content -->
			<!-- Page Content End -->
			<?php include("includes/common/footer.php"); ?>
		</div>
	</div>
</body>
</html>